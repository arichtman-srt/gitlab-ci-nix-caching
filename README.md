# GitLab CI Nix Caching

Simple flake and CI demonstrating build and run using GitLab caching.
Everything is [presently broken](https://github.com/NixOS/nix/issues/8289), unfortunately.

Deliberately set to only x86_64 linux for simplicity.

## References

- https://kevincox.ca/2022/01/02/nix-in-docker-caching/
- https://gitlab.com/999eagle/nur-packages/-/blob/main/.gitlab-ci.yml
- https://git.catgirl.cloud/999eagle/dotfiles-nix/-/commit/4b9c8cb5f4e632a0b507263cb2d8a9352ca4590b
- https://mastodon.catgirl.cloud/@sophie/112732934073775604
- https://mastodon.catgirl.cloud/@sophie/112732988248410996
- https://mastodon.catgirl.cloud/@sophie/112733009794069084
- https://mastodon.catgirl.cloud/@sophie/112733051346778293
- https://mastodon.catgirl.cloud/@sophie/112733070072482284
