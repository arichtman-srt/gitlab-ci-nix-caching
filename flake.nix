{
  description = "Development environment";
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/1afc5440469f94e7ed26e8648820971b102afdc3";
  outputs = { nixpkgs, ... }:
let
    pkgs = nixpkgs.legacyPackages.x86_64-linux;
in {
  packages.x86_64-linux.yoohoo = pkgs.hello;

  # devShells.x86_64-linux.default = pkgs.mkShell {
  #   nativeBuildInputs = [ pkgs.cowsay ];
  # };
};
}

